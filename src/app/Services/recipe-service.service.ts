import { Injectable } from '@angular/core';
import { Receipt } from '../receipts/receipt';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class RecipeServiceService {

  private recipe = new BehaviorSubject<Receipt>(new Receipt("Unknown"));

  currentRecipe = this.recipe.asObservable();

  constructor() { }

  changeRecipe(newRecipe: Receipt) {
    this.recipe.next(newRecipe);
  }
  
}
