import { Component, OnInit } from '@angular/core';
import { Receipt } from './receipt';
import { Ingredient } from '../ingredients/ingredient';
import { RecipeServiceService } from '../Services/recipe-service.service';

@Component({
  selector: 'receipts',
  templateUrl: './receipts.component.html',
  styleUrls: ['./receipts.component.css']
})
export class ReceiptsComponent implements OnInit {

  //
  // Attributes
  recipeList : Receipt[];
  recipe: Receipt;
  detailsHidden: boolean = true;
  
  //
  // Constructor
  constructor(private data: RecipeServiceService) { 
    this.recipeList = [
      new Receipt("Huevos a la flamenca", [
                          new Ingredient("Huevo",null),
                          new Ingredient("Tomate",null),
                          new Ingredient("Orégano",null)
                      ],
                      "../../assets/huevos-flamenca.jpg"),
      new Receipt("Salmorejo", [
                          new Ingredient("Tomate",null),
                          new Ingredient("Cebolla",null),
                          new Ingredient("Pimiento",null)
                      ]),
      new Receipt("Tortilla de patatas")
    ]
  }


  //
  // Other methods

  // Show Details
  showDetails(recipe: Receipt) {

    if(this.detailsHidden) { // Hidden -> show
      this.data.changeRecipe(recipe);
      this.detailsHidden = false;

    } else if(!this.detailsHidden && recipe.title == this.recipe.title) { // Visible and same Recipe -> hide
      this.detailsHidden = true;

    } else { // Load new Recipe
      this.data.changeRecipe(recipe);
    }

  }
  // Add
  addReceipt(title: String) {
    var newRecipe: Receipt = new Receipt(title);

    if(title.trim() != "" && !this.contains(newRecipe))
      this.recipeList.push(newRecipe);
  }

  // Contains
  contains(receipt: Receipt) {
    
    for (let i = 0; i < this.recipeList.length; i++) {
      if(this.recipeList[i].title.trim() == receipt.title.trim())
        return true;
    }
    return false;
  }

  // Show / hide ingredients
  toggleIngredients(recipe: Receipt) {
      recipe.collapsed = !recipe.collapsed;
  }

  ngOnInit() {
    this.data.currentRecipe.subscribe(recipe => this.recipe = recipe);
  }

}
