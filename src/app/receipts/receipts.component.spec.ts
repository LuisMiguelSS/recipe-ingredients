
import { ReceiptsComponent } from './receipts.component';
import { Receipt } from './receipt';
import { RecipeServiceService } from '../Services/recipe-service.service';

describe('ReceiptsComponent', () => {
  let recipesComponent: ReceiptsComponent;

  beforeEach(() => {
    recipesComponent = new ReceiptsComponent(new RecipeServiceService());
    recipesComponent.ngOnInit();
  });

  it('Creación del componente', () => {
    expect(recipesComponent).toBeTruthy();
  });

  it('Añadir receta', () => {
    recipesComponent.addReceipt("Mi receta");
    expect(recipesComponent.recipeList).toContain(new Receipt("Mi receta"));
  });
});
