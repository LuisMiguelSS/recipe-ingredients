import { Ingredient } from '../ingredients/ingredient';

export class Receipt {

    // Attributes
    title: String;
    ingredientList: Ingredient[];
    collapsed: boolean;
    img: String;

    // Constructor
    constructor(title: String, ingredientList: Ingredient[] = [], img: String = "") {
        this.title = title;
        this.ingredientList = ingredientList;
        this.collapsed = true;
        this.img = img;
    }
}
