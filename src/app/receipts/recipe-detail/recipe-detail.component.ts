import { Component, OnInit, Input } from '@angular/core';
import { Receipt } from '../../receipts/receipt';
import { RecipeServiceService } from 'src/app/Services/recipe-service.service';
import { Ingredient } from 'src/app/ingredients/ingredient';

@Component({
  selector: 'recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {

  //
  // Attrs
  recipe: Receipt;

  //
  // Constr
  constructor(private data: RecipeServiceService) { }

  ngOnInit() {
    this.data.currentRecipe.subscribe(recipe => this.recipe = recipe);
  }

  //
  // Other methods

  // Add
  AddNewIngredient() {
    this.recipe.ingredientList.push(new Ingredient("New ingredient"));
  }

  // Delete
  DeleteIngredient(ingr: Ingredient) {
    var index: number = this.recipe.ingredientList.indexOf(ingr);

    if (index !== -1)
        this.recipe.ingredientList.splice(index, 1);
  }

  // Update
  UpdateIngredient(anterior: String, nuevo: String) {
    this.recipe.ingredientList.forEach(ingr => {
      if(ingr.name == anterior)
        ingr.name = nuevo
    });
  }

}
