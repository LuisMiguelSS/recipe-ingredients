import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { ReceiptsComponent } from './receipts/receipts.component';
import { IngredientsComponent } from './ingredients/ingredients.component';
import { RecipeDetailComponent } from './receipts/recipe-detail/recipe-detail.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { HomeComponent } from './home/home.component';
import { RecipeCustomizerPipe } from './CustomPipes/recipe-customizer.pipe';
import { HighlighterDirective } from './CustomDirectives/highlighter.directive';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'recetas', redirectTo: 'recipes' },
  { path: 'ingredientes', redirectTo: 'ingredients'},

  { path: 'recipes', component: ReceiptsComponent },
  { path: 'ingredients', component: IngredientsComponent },

  { path: '**', component: NotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    MainMenuComponent,
    ReceiptsComponent,
    IngredientsComponent,
    RecipeDetailComponent,
    NotFoundComponent,
    HomeComponent,
    RecipeCustomizerPipe,
    HighlighterDirective
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
