export class Ingredient {

    // Attributes
    name: String;
    image: String = "";

    // Contructor
    constructor(name: String, image: String = "assets/unknown.png") {
        this.name = name;
        this.image = image;
    }
}
