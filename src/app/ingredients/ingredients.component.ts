import { Component, OnInit } from '@angular/core';
import { Ingredient } from './ingredient';

@Component({
  selector: 'ingredients',
  templateUrl: './ingredients.component.html',
  styleUrls: ['./ingredients.component.css']
})
export class IngredientsComponent implements OnInit {

  //
  // Attrs
  ingredientList: Ingredient[];

  //
  // Constr
  constructor() {
  }

  ngOnInit() {
  }


}
