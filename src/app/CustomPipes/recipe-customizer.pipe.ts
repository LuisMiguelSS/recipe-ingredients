import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'recipeCustomizer'
})
export class RecipeCustomizerPipe implements PipeTransform {

  transform(recipeTitle: String, ...args: any[]): any {
    return recipeTitle + " - My Recipes";
  }

}
