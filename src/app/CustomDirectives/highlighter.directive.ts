import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appHighlighter]'
})
export class HighlighterDirective {
  
  //
  // Constr
  constructor(private elem: ElementRef) {
  }

  //
  // Other methods
  @HostListener('mouseenter') onMouseEnter() {
    this.highlight('lightgrey');
  }
  
  @HostListener('mouseleave') onMouseLeave() {
    this.highlight(null);
  }
  
  private highlight(color: string) {
    this.elem.nativeElement.style.backgroundColor = color;
  }

}
