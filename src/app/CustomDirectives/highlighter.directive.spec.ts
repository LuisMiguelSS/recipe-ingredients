import { HighlighterDirective } from './highlighter.directive';
import { ElementRef } from '@angular/core';

describe('HighlighterDirective', () => {
  it('should create an instance', () => {
    const directive = new HighlighterDirective(new ElementRef(""));
    expect(directive).toBeTruthy();
  });
});
